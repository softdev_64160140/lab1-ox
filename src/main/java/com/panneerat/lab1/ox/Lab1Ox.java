/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.panneerat.lab1.ox;

import java.util.Scanner;

/**
 *
 * @author hp
 */
public class Lab1Ox {

    private char[][] board;
    private char player;

    public Lab1Ox() {
        board = new char[3][3];
        player = 'X';
        
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = '-';
            }
        }
    }



    public void printBoard() {
        System.out.println("-------------");
        for (int i = 0; i < 3; i++) {
            System.out.print("| ");
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " | ");
            }
            System.out.println();
            System.out.println("-------------");
        }
    }

    //เช็คเสมอ
    public boolean isBoardFull() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }
//เช็คคนชนะ

    public boolean isWinner() {
        //เช็คแนวนอน
        for (int i = 0; i < 3; i++) {
            if (board[i][0] == player && board[i][1] == player && board[i][2] == player) {
                return true;
            }
        }

        //เช็คแนวตั้ง
        for (int j = 0; j < 3; j++) {
            if (board[0][j] == player && board[1][j] == player && board[2][j] == player) {
                return true;
            }
        }

        //เช็คแทยง
        if (board[0][0] == player && board[1][1] == player && board[2][2] == player) {
            return true;
        }
        if (board[0][2] == player && board[1][1] == player && board[2][0] == player) {
            return true;
        }

        return false;
    }

    public void changePlayer() {
        player = (player == 'X') ? 'O' : 'X';
    }

    public boolean makeMove(int row, int col) {
        if (row < 0 || row >= 3 || col < 0 || col >= 3 || board[row][col] != '-') {
            return false;
        }

        board[row][col] = player;
        return true;
    }

    public static void main(String[] args) {
        Lab1Ox game = new Lab1Ox();
        Scanner s = new Scanner(System.in);

        while (true) {
            System.out.println("Welcome to Ox game");
            System.out.println("Start!!");
            game.printBoard();
            System.out.print("player " + game.player + " which row and column do you choose? : ");
            int row = s.nextInt() - 1;
            int col = s.nextInt() - 1;

            if (game.makeMove(row, col)) {
                game.printBoard();
                if (game.isWinner()) {
                    System.out.println("player " + game.player + "  win!!!");
                    break;
                } else if (game.isBoardFull()) {
                    System.out.println("It's a draw!");
                    break;
                } else {
                    game.changePlayer();
                }
            } else {
                System.out.println("Invalid move! try again.");
            }
        }
        
        s.close();
    }

}
